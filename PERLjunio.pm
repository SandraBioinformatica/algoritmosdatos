#Sandra Conejo Quiles

#Bloque 1

use strict;
use warnings;
use List::Util qw(max min);
use List::MoreUtils "none";
use lib(".");
use variaciones;

sub fusionar{  #se crea la subrutina fusionar con el fin de devolver un hash con información sobre el solapamiento de dos cadenas
	my ($cadena1, $cadena2) = @_;  #los parámetros de la subrutina
	
	my %h_resultado = (
    "cadena1" => $cadena1,
    "cadena2" => $cadena2,
    "solapamiento" => 0,
    "fusion" => $cadena1.$cadena2,
    );  #se crea el hash que se nos pide como resultado, con la clave y los valores iniciales
    
	my $len1 = length($cadena1);
	my $len2 = length($cadena2);
	my @list = ($len1, $len2);
	my $longmin = min @list;  #se hace el mínimo de la longitud de las cadenas pasadas, ya que el tamaño de la cadena más pequeña indicará el número de iteraciones en el bucle
	for (my $i = 1; $i <= $longmin; $i++){ #siendo el índice 1 como valor de inicio, mientras $i sea menor o igual que el número entero que se ha obtenido de la longitud mínima,
											#el bucle seguirá funcionando. 
		my $sufijo = substr($cadena1,-$i);  #en la variable sufijo se comienza por el final de la cadena1 y se va desplazando hacia la izquierda
		#print $sufijo,"\n";
        my $prefijo = substr($cadena2, 0, $i); #en cambio la variable prefijo comienza por el inicio de la cadena2 y se va desplazando hacia la derecha
        #print $prefijo,"\n";
        my $trozodecadena = substr($cadena1,0,$len1-$i); #se crea otra variable que almacena el restante de la cadena1 tras quitarle el sufijo en cada iteración
        #$trozodecadena,"\n";

		if ($sufijo eq $prefijo) {  #en cada iteración se comprueba si se cumple la condición de igualdad entre "prefijo" y "sufijo" y si es así:
			$h_resultado{"solapamiento"} = $i;  #se actualiza el hash creado anteriormente, la clave "solapamiento" tendrá como valor el número de bases que solapan
			$h_resultado{"fusion"} = $trozodecadena.$cadena2;	#también se actualiza la clave "fusion" por la cadena fusionada, que se obtiene concatenando trozodecadena y cadena2.
		}
    }
    return %h_resultado;  #la subrutina devuelve un hash
}



sub fusionar_lista{  #se define la subrutina fusionar_lista cuya tarea será fusionar listas
	my @cadenas = @_;  #parámetros
	my $cadenafusionada = '';  #se crea una lista vacía para inicializar la variable $cadenafusionada
	foreach my $cadena (@cadenas) { #con un bucle foreach se recorre la lista de cadenas y para cada una de ellas:
		my %dic = fusionar($cadenafusionada, $cadena); #se invoca la función "fusionar" pasando como parámetros la variable inicializada y cada una de las cadenas que se introducen por el usuario
		$cadenafusionada = $dic{"fusion"}; #el ejercicio nos pide devolver una cadena, con lo cual accedemos al diccionario y seleccionamos "fusion" que contiene el resultado que nos interesa.
	}
	return $cadenafusionada;  #la subrutina devuelve una cadena de texto
}



#Bloque 2
sub ensamblar_busqueda_exhaustiva{
	my @fragmentos = @_;  
	my @permu = @fragmentos;  #inicializo la variable @permu con el valor de @fragmentos
	my $cadresult = fusionar_lista(@permu); #inicializo la variable $cadresult con la fusión de @permu
	while (@permu) {
		my $cadfusion = fusionar_lista(@permu);

		if (length($cadresult) > length($cadfusion)){  #si la cadena recién creada es más pequeña que la almacenada en $cadresult,
														#se actualiza el valor de ésta
			$cadresult = $cadfusion;
		}

		@permu = permutar @fragmentos, @permu; #se genera la siguiente permutación
												#si es la última permutación resulta una cadena vacía y acaba el bucle
	}
	return $cadresult;
}


#Bloque 3
sub ensamblar_avance_rapido{
	my @fragmentos = @_;
	my @permu = @fragmentos;
	my %dic; 
	
	while (@permu) {
		my %dic_fusionar = fusionar($permu[0],$permu[1]);  #se obtiene el diccionario al fusionar las dos primeras cadenas de @permu
		my $clave = $dic_fusionar{"fusion"}; #se utiliza "fusion" como clave ya que a priori es única
		$dic{$clave} = \%dic_fusionar; #se introduce %dic_fusionar a %dic pasándolo por referencia, creando un diccionario de diccionarios
		
		@permu = permutar @fragmentos, @permu;
	}
	my @orden_solapamiento = sort { $dic{$b}{"solapamiento"} <=> $dic{$a}{"solapamiento"} } keys %dic; 
	#en @orden_solapamiento estarán las claves ordenadas de mayor a menor por solapamiento 
	
	
	my @fragsordenados = (); #inicializo el array de fragmentos a fusionar
	foreach my $clavefus (@orden_solapamiento){
		my $cadena1 = $dic{$clavefus}{"cadena1"};
		my $cadena2 = $dic{$clavefus}{"cadena2"};

		if ((none { $_ eq $cadena1 } @fragsordenados) and (none { $_ eq $cadena2 } @fragsordenados)) { 
			#si ni $cadena1 ni $cadena2 se encuentran en @fragsordenados, se introducen en el array @fragsordenados
			push @fragsordenados, $cadena1;
			push @fragsordenados, $cadena2;
		}	
	}
	foreach my $frag (@fragmentos){  #si falta un fragmento por introducir (número impar de @fragmentos de entrada) se introduce
		if (none { $_ eq $frag } @fragsordenados){
			push @fragsordenados, $frag;
		}
	}
		   
	return fusionar_lista(@fragsordenados);
}

1;