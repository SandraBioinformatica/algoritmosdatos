#Sandra Conejo Quiles

use strict;
use warnings;
use List::Util qw(max min);
use lib(".");
use variaciones;
use PERLjunio;

#Bloque 1
print "Ejercicio 1:\n";
my %resultado = fusionar("AACCTG", "TGAAACTCA");
my @keys = keys %resultado;
foreach my $k (@keys){
	print $k, " : ", $resultado{$k}, "\n";
}

print "\n";

print "Ejercicio 2:\n";
my $cadenafusionada = fusionar_lista("AACCTG", "TGAAACTCA", "ACCA");
print "La cadena fusionada resultante es: ", $cadenafusionada,"\n\n";

#Bloque 2
print "Ejercicio 3:\n";
my @fragmentos = ("CTGACAA","CCGCAATC","CTAAGT","AAAGCTA","TCTCCCA");
my $fusion = ensamblar_busqueda_exhaustiva(@fragmentos);
print "La cadena que contiene todos los fragmentos y es la más corta resultado del proceso de fusión es: ", $fusion,"\n";
print "La longitud de la cadena resultante es: ", length($fusion), "\n\n";

#Bloque 3
print "Ejercicio 4:\n";
@fragmentos = ("CTGACAA","CCGCAATC","CTAAGT","AAAGCTA","TCTCCCA");
$fusion = ensamblar_avance_rapido(@fragmentos);
print "La cadena que contiene todos los fragmentos tras la fusión es: ", $fusion,"\n";
print "La longitud de la cadena resultante es: ", length($fusion);